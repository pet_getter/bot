import collections
import logging
import os
from zipfile import ZipFile

import aiofiles
import aiohttp
import config
from aiofiles.os import remove as aio_remove
from bot import async_bot
from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    KeyboardButton,
    Message,
    ReplyKeyboardMarkup,
)

logger = logging.getLogger(__name__)


def get_tg_id_from_message(message: "Message") -> int:
    return message.from_user.id


def generate_markup(request_user_id: int = None) -> "ReplyKeyboardMarkup":
    """Generate markup for messages"""
    markup: ReplyKeyboardMarkup = ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    get_cat_button: "KeyboardButton" = KeyboardButton(config.GET_CAT_BUTTON)
    get_dog_button: "KeyboardButton" = KeyboardButton(config.GET_DOG_BUTTON)
    markup.add(*[get_cat_button, get_dog_button])

    show_my_collection: "KeyboardButton" = KeyboardButton(config.MY_COLLECTION)
    edit_collection: "KeyboardButton" = KeyboardButton(config.EDIT_COLLECTION)
    markup.add(show_my_collection, edit_collection)

    if request_user_id == config.ADMIN_USER_ID:
        user_list_button: "KeyboardButton" = KeyboardButton(config.USER_LIST_BUTTON)
        markup.add(user_list_button)

    return markup


def generate_inline_markup() -> "InlineKeyboardMarkup":
    markup: "InlineKeyboardMarkup" = InlineKeyboardMarkup()
    return markup


async def get_file_async(client: "aiohttp.ClientSession", url: str) -> bytes:
    response: "aiohttp.ClientResponse" = await client.get(url)
    return await response.read()


async def extract_zip_file(
    file_path: str, user_id: int
) -> "collections.AsyncGenerator":
    extract_to: str = os.path.join(config.TEMP_FOLDER, str(user_id))
    with ZipFile(file_path) as zipObj:
        file_names: list = zipObj.namelist()
        for file_name in file_names:
            yield zipObj.extract(file_name, extract_to)
    await aio_remove(file_path)


async def get_files_from_zip(
    file_path: str, user_id: int
) -> "collections.AsyncGenerator":
    async for file_name in extract_zip_file(file_path, user_id):
        async with aiofiles.open(file_name, mode="rb") as f:
            file = await f.read()
        yield file


def generate_collection_markup(
    file_index: int, file_name: str, collection_len: int
) -> "InlineKeyboardMarkup":
    markup: "InlineKeyboardMarkup" = generate_inline_markup()

    prev_button: "InlineKeyboardButton" = InlineKeyboardButton(
        text="prev",
        callback_data=f"next_collection{config.CB_SEP}{(file_index - 1) % collection_len}",
    )

    remove_button: "InlineKeyboardButton" = InlineKeyboardButton(
        text="remove",
        callback_data=f"remove_collection{config.CB_SEP}{file_name}{config.CB_SEP}{file_index}",
    )
    next_button: "InlineKeyboardButton" = InlineKeyboardButton(
        text="next",
        callback_data=f"next_collection{config.CB_SEP}{(file_index + 1) % collection_len}",
    )

    markup.add(prev_button, remove_button, next_button)
    return markup


def get_or_create_user_temp_folder(user_id: int) -> str:
    file_path: str = os.path.join(config.TEMP_FOLDER, str(user_id))
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    return file_path


async def get_files_for_collection(
    message: "Message", file_index: int, from_prev: bool
) -> list[str]:
    files: list = []
    if file_index or from_prev:
        collection_dir: str = get_or_create_user_temp_folder(message.chat.id)
        files = [
            os.path.join(collection_dir, file)
            for file in os.listdir(collection_dir)
            if os.path.isfile(os.path.join(collection_dir, file))
        ]
    else:
        archive_name: str = await async_bot.get_collection(message)
        async for file in extract_zip_file(archive_name, message.from_user.id):
            files.append(file)
    return files
