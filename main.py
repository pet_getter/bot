import asyncio
import logging

import config
import sentry_sdk
from bot import async_bot

logger = logging.getLogger(__name__)

if sentry_token := config.SENTRY_URL:
    sentry_sdk.init(dsn=sentry_token, traces_sample_rate=1.0)

if __name__ == "__main__":
    asyncio.run(
        async_bot.polling(timeout=40, none_stop=True, request_timeout=300),
    )
