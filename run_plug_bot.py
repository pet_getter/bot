import asyncio
import logging

from bot import plug_bot

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    asyncio.run(plug_bot.infinity_polling())
