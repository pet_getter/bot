import logging.config
import os
import typing

from dotenv import load_dotenv

load_dotenv()

TOKEN: str = os.environ.get("BOT_TOKEN")
assert TOKEN, "No bot token in .env file!"

APP_ROOT: str = os.path.dirname(os.path.abspath(__file__))
CAT_URL: str = "https://cataas.com/cat"
DOG_URL: str = "https://dog.ceo/api/breeds/image/random"

ADMIN_USER_ID: int = int(os.environ.get("ADMIN_ID"))

SENTRY_URL: typing.Optional[str] = os.environ.get("SENTRY_URL")

PET_MEDIA_ROOT: str = os.path.join(APP_ROOT, "media/pet_images")

MY_COLLECTION: str = "My Collection"

EDIT_COLLECTION: str = "Edit Collection"

GET_CAT_BUTTON: str = "Get Cat!"
GET_DOG_BUTTON: str = "Get Dog!"
USER_LIST_BUTTON: str = "User List"

TEMP_FOLDER: str = os.path.join(APP_ROOT, "tmp")

CB_SEP: str = ":++:"

EMPTY_COLLECTION = "Empty collection :("

if not os.path.exists(TEMP_FOLDER):
    os.makedirs(TEMP_FOLDER)

HOST: str = os.environ.get("HOST", 20)
PORT: str = os.environ.get("PORT", 20)

FORMAT: str = "%(levelname)s | %(name)s | %(asctime)s | %(lineno)d | %(message)s"
LOGGER_LEVEL: int = int(os.environ.get("LOGGER_LEVEL", 20))

dict_config: dict = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {"base": {"format": FORMAT}},
    "handlers": {
        "file": {
            "class": "logger.LevelFileHandler",
            "level": LOGGER_LEVEL,
            "formatter": "base",
            "when": "d",
            "interval": 1,
        },
        "stream": {
            "class": "logging.StreamHandler",
            "level": LOGGER_LEVEL,
            "formatter": "base",
        },
    },
    "loggers": {
        "": {
            "level": LOGGER_LEVEL,
            "handlers": ["file", "stream"],
            "propagate": False,
        }
    },
}
logging.config.dictConfig(dict_config)
