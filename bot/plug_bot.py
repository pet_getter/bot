import os
import random

import config
from telebot.async_telebot import AsyncTeleBot
from telebot.types import Message

plug_bot = AsyncTeleBot(config.TOKEN)


@plug_bot.message_handler(
    func=lambda message: True,
    content_types=[
        "audio",
        "photo",
        "voice",
        "video",
        "document",
        "text",
        "location",
        "contact",
        "sticker",
    ],
)
async def plug(message: "Message") -> None:
    sorry_text = "Engineering works, Bark! Bark!"
    plug_pictures_folder = os.path.join(config.APP_ROOT, "media/plugs")
    plug_files = os.listdir(plug_pictures_folder)
    file_to_send = random.choice(plug_files)
    full_file_path = os.path.join(plug_pictures_folder, file_to_send)
    with open(full_file_path, "rb") as file:
        file_to_send_bytes: bytes = bytearray(file.read())
    await plug_bot.send_photo(
        chat_id=message.chat.id, photo=file_to_send_bytes, caption=sorry_text
    )
