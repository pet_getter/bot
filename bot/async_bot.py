import json
import logging
import os
import typing
from datetime import datetime
from pathlib import Path

import aiofiles
import aiohttp
import config
from aiofiles.os import remove as aio_remove
from bot import utils
from telebot.async_telebot import AsyncTeleBot
from telebot.types import (
    CallbackQuery,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    InputMediaPhoto,
    Message,
    ReplyKeyboardMarkup,
)

async_bot = AsyncTeleBot(config.TOKEN)
OUT_PATH = Path(__file__).parent / "cats"
logger = logging.getLogger(__name__)


@async_bot.message_handler(commands=["start"])
async def start_message(message: "Message") -> "Message":
    """Send start message"""
    logger.debug(
        f"get message: {message.text}, from: {message.from_user.id}:{message.from_user.username}"
    )
    markup: "ReplyKeyboardMarkup" = utils.generate_markup(message.from_user.id)
    text = f"Hello {message.from_user.full_name}, push button to get a cat or dog!"
    return await async_bot.send_message(message.chat.id, text, reply_markup=markup)


async def get_collection(message: "Message") -> str:
    url: str = (
        f"{config.HOST}:{config.PORT}/api/user/user_images/{message.from_user.id}"
    )

    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(60)) as client:
        response = await client.get(url)
        content: bytes = await response.read()
        file_name: str = f"{message.from_user.id}_{str(datetime.now())}.zip"
        file_path: str = utils.get_or_create_user_temp_folder(message.from_user.id)

        full_file_name: str = os.path.join(file_path, file_name)
        async with aiofiles.open(full_file_name, mode="wb") as f:
            await f.write(content)
    return full_file_name


@async_bot.message_handler(regexp=config.MY_COLLECTION)
async def my_collection(message: "Message") -> typing.Union[list["Message"], "Message"]:
    """Get user's collection (async)"""
    media_group: list[InputMediaPhoto] = list()
    archive_name: str = await get_collection(message)
    async for file in utils.get_files_from_zip(archive_name, message.from_user.id):
        media_group.append(InputMediaPhoto(file))
    if media_group:
        return await async_bot.send_media_group(message.chat.id, media_group)
    else:
        return await async_bot.send_message(message.chat.id, config.EMPTY_COLLECTION)


@async_bot.message_handler(regexp=config.EDIT_COLLECTION)
async def edit_collection(
    message: "Message", file_index: int = 0, from_prev: bool = False
) -> "Message":
    files: list = await utils.get_files_for_collection(message, file_index, from_prev)

    if files:
        current_file: str = files[file_index]

        async with aiofiles.open(current_file, mode="rb") as file:
            content = await file.read()
        file_name: str = current_file.split(os.sep)[-1]
        markup = utils.generate_collection_markup(file_index, file_name, len(files))
        return await async_bot.send_photo(message.chat.id, content, reply_markup=markup)
    else:
        return await async_bot.send_message(message.chat.id, config.EMPTY_COLLECTION)


@async_bot.callback_query_handler(
    func=lambda callback: callback.data.startswith("next_collection")
)
async def next_collection(callback: "CallbackQuery") -> "Message":
    next_picture_index = int(callback.data.split(config.CB_SEP)[-1])
    from_prev = next_picture_index == 0
    return await edit_collection(callback.message, next_picture_index, from_prev)


@async_bot.callback_query_handler(
    func=lambda callback: callback.data.startswith("remove_collection")
)
async def remove_from_collection(callback: "CallbackQuery") -> "Message":
    _, file_name, file_index_str = callback.data.split(config.CB_SEP)
    file_index: int = int(file_index_str)
    url: str = f"{config.HOST}:{config.PORT}/api/user/user_images/remove"
    data: dict = {"tg_id": callback.message.chat.id, "file_name": file_name}
    headers: dict = {"Content-Type": "application/json"}
    data_json = json.dumps(data)

    async with aiohttp.ClientSession(
        timeout=aiohttp.ClientTimeout(60), headers=headers
    ) as client:
        response = await client.post(url, data=data_json)
        content: bytes = await response.read()
        content_json = json.loads(content)
    user_temp_folder: str = utils.get_or_create_user_temp_folder(
        callback.message.chat.id
    )
    if content_json["success"]:
        await aio_remove(os.path.join(user_temp_folder, file_name))
    await async_bot.send_message(
        callback.message.chat.id,
        content_json["message"],
        reply_to_message_id=callback.message.message_id,
    )

    user_files: list = os.listdir(user_temp_folder)
    user_files_len: int = len(user_files)
    if user_files_len == 0:
        return await async_bot.send_message(
            callback.message.chat.id, config.EMPTY_COLLECTION
        )
    return await edit_collection(
        callback.message, file_index % user_files_len, from_prev=True
    )


async def sender(message: "Message", url: str, pet: str) -> "Message":
    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(60)) as client:
        response: "aiohttp.ClientResponse" = await client.get(url)
        content = await response.read()
        increase_url = f"{config.HOST}:{config.PORT}/api/user/increase"
        data = {
            "id": message.from_user.id,
            "first_name": message.from_user.first_name,
            "last_name": message.from_user.last_name,
            "username": message.from_user.username,
        }
    headers = {"Content-Type": "application/json"}
    async with aiohttp.ClientSession(
        timeout=aiohttp.ClientTimeout(60), headers=headers
    ) as client:
        data_json = json.dumps(data)
        await client.post(increase_url, data=data_json)

    markup: "InlineKeyboardMarkup" = utils.generate_inline_markup()
    add_button: "InlineKeyboardButton" = InlineKeyboardButton(
        text="Add to Collection", callback_data=f"add"
    )
    markup.add(add_button)
    logger.debug(
        f"sent {pet}'s pic to: {message.from_user.id}:{message.from_user.username}"
    )
    return await async_bot.send_photo(message.chat.id, content, reply_markup=markup)


@async_bot.message_handler(regexp=config.GET_CAT_BUTTON)
async def get_cat_bot(message: "Message") -> "Message":
    """Send cat picture (async)"""
    url: str = f"{config.HOST}:{config.PORT}/api/cat"
    return await sender(message, url, "cat")


@async_bot.message_handler(regexp=config.GET_DOG_BUTTON)
async def get_dog_bot(message: "Message") -> "Message":
    """Send dog picture (async)"""
    url: str = f"{config.HOST}:{config.PORT}/api/dog"
    return await sender(message, url, "dog")


async def _get_user_list() -> tuple[str, "InlineKeyboardMarkup"]:
    url: str = f"{config.HOST}:{config.PORT}/api/user"
    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(60)) as client:
        response: "aiohttp.ClientResponse" = await client.get(url)
        users_raw: bytes = await response.read()
    users: list = json.loads(users_raw)
    text = "All Users:"
    markup: "InlineKeyboardMarkup" = utils.generate_inline_markup()
    for user in users:
        markup.add(
            InlineKeyboardButton(
                text=f"User: username: {user['username']}",
                callback_data=f"user{config.CB_SEP}{user['tg_id']}",
            )
        )
    return text, markup


@async_bot.message_handler(
    regexp=config.USER_LIST_BUTTON,
    func=lambda message: message.from_user.id == config.ADMIN_USER_ID,
)
async def get_user_list(message: "Message") -> "Message":
    """get user list (for admin only)"""
    text, markup = await _get_user_list()
    return await async_bot.send_message(message.chat.id, text, reply_markup=markup)


@async_bot.callback_query_handler(func=lambda call: call.data.startswith("user"))
async def user_callback(call: "CallbackQuery") -> "Message":
    callback_data: list = call.data.split(config.CB_SEP)
    inline_markup: "InlineKeyboardMarkup" = utils.generate_inline_markup()
    user_id: int = int(callback_data[1])
    url: str = f"{config.HOST}:{config.PORT}/api/user/{user_id}"
    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(60)) as client:
        response: "aiohttp.ClientResponse" = await client.get(url)
        user: bytes = await response.read()
    user: dict = json.loads(user)
    text: str = f"""
    User's data:
    id: {user['tg_id']}
    username: {user['username']}
    first name: {user['first_name']}
    last name: {user['last_name']}
    requests: {user['requests']}
    """
    back_button: "InlineKeyboardButton" = InlineKeyboardButton(
        text=f"Back",
        callback_data=f"list_user",
    )
    inline_markup.add(back_button)
    context: dict = {
        "text": text,
        "chat_id": call.message.chat.id,
        "message_id": call.message.message_id,
        "reply_markup": inline_markup,
    }
    return await async_bot.edit_message_text(**context)


@async_bot.callback_query_handler(func=lambda call: call.data.startswith("list_user"))
async def user_list(call: "CallbackQuery") -> "Message":
    text, markup = await _get_user_list()
    return await async_bot.edit_message_text(
        chat_id=call.message.chat.id,
        message_id=call.message.message_id,
        text=text,
        reply_markup=markup,
    )


async def get_photo_async(photo_id: str) -> dict:
    async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(60)) as client:
        file_data_url: str = (
            f"https://api.telegram.org/bot{config.TOKEN}/getFile?file_id={photo_id}"
        )
        pre_response: bytes = await utils.get_file_async(client, file_data_url)
        file_data: dict = json.loads(pre_response)
        result: dict = file_data["result"]
        return result


@async_bot.callback_query_handler(func=lambda callback: callback.data.startswith("add"))
async def add_picture_callback(call: "CallbackQuery") -> "Message":
    photo_index: int = len(call.message.photo)
    photo_id: str = call.message.photo[photo_index - 1].file_id
    photo_data: dict = await get_photo_async(photo_id)
    message: str = "Failed to save file"

    file_path: str = photo_data["file_path"]
    file_id: str = photo_data["file_unique_id"]

    get_url_path: str = f"{config.HOST}:{config.PORT}/api/image/save"
    headers: dict = {"Content-Type": "application/json"}
    data: dict = {
        "file_id": file_id,
        "src_link": f"https://api.telegram.org/file/bot{config.TOKEN}/{file_path}",
        "user_id": call.from_user.id,
    }
    try:
        async with aiohttp.ClientSession(
            timeout=aiohttp.ClientTimeout(60), headers=headers
        ) as client:
            data_json: str = json.dumps(data)
            response: "aiohttp.ClientResponse" = await client.post(
                get_url_path, data=data_json
            )
            creation_data_raw: bytes = await response.read()
            creation_data: dict = json.loads(creation_data_raw)
            message = creation_data.get("message")
    except Exception as exc:
        logger.error(exc)
    return await async_bot.send_message(
        call.message.chat.id, message, reply_to_message_id=call.message.message_id
    )
